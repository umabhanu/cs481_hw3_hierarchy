﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw2
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage()); // mainpage is root page and set as first navigation page
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
