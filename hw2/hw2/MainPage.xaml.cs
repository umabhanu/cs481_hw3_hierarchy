﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace hw2
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }


        private async void First_Button(object sender, EventArgs e)
        {  
           await Navigation.PushAsync(new Page1()); // this method opens page 1 when it is executed
        }

        private async void Second_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2()); //opens page 2
        }


         private async void Third_Button(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3()); //opens page 3
        }
    }
}
