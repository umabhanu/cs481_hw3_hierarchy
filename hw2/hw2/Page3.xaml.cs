﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace hw2
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }
         async void Sixth_Button(object sender, EventArgs e)
        {
            bool usersResponse = await DisplayAlert("hello", "do you want to go to main page" ,"yes" , "no"); // alert message is seen and when we press yes it executes next statement
            if (usersResponse == true)
            {
                await Navigation.PopToRootAsync(); // goes back to root page when poptoasync method executes
            }
        }
    }
}